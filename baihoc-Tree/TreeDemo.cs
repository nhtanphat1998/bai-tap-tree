﻿namespace baihoc_Tree
{
    
    public class TreeDemo
    {
        public Node Root { get; set; }
        public void InsertNode(int data)
        {
            if (Root == null)
            {
                Root = new Node();
                Root.Data = data;
            }
            else
            {
                AddNodeRecursively(Root, data);
            }
        }

        public void AddNodeRecursively(Node currentNode, int dataOfNewNode)
        {
            try
            {
                if (dataOfNewNode > currentNode.Data)
                {
                    if (currentNode.RightNode == null)
                    {
                        currentNode.RightNode = new Node();
                        currentNode.RightNode.Data = dataOfNewNode;
                    }
                    else
                    {
                        AddNodeRecursively(currentNode.RightNode, dataOfNewNode);
                    }
                }
                else if (dataOfNewNode < currentNode.Data)
                {
                    if (currentNode.LeftNode == null)
                    {
                        currentNode.LeftNode = new Node();
                        currentNode.LeftNode.Data = dataOfNewNode;
                    }
                    else
                    {
                        AddNodeRecursively(currentNode.LeftNode, dataOfNewNode);
                    }
                }
                else
                {
                    throw new Exception("Giá trị không được bằng nhau trong tree");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }

    public class Node
    {
        public int Data { get; set; }
        public Node LeftNode;
        public Node RightNode;
    }
}
